import 'package:flutter/cupertino.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:ktsurf/models/spot.dart';
import 'package:share/share.dart';

class CupertinoDetailsScreen extends StatelessWidget {
  final Spot spotInfo;

  CupertinoDetailsScreen({Key key, this.spotInfo}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CupertinoPageScaffold(
        navigationBar: CupertinoNavigationBar(
            backgroundColor: CupertinoColors.extraLightBackgroundGray,
            previousPageTitle: 'Spots',
            middle: Text(
              spotInfo.name,
              style: TextStyle(
                fontFamily: 'Georgia',
                fontWeight: FontWeight.normal,
                fontSize: 18,
              ),
              textAlign: TextAlign.center,
            ),
        ),
        child: Padding(
          padding: EdgeInsets.fromLTRB(
              MediaQuery.of(context).size.width * 0.05,
              MediaQuery.of(context).size.height * 0.025,
              MediaQuery.of(context).size.width * 0.05,
              0),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  DetailsListItem(
                    height: MediaQuery.of(context).size.height * 0.1,
                    width: MediaQuery.of(context).size.width * 0.4,
                    title: 'Longitude',
                    value: spotInfo.longitude,
                  ),
                  DetailsListItem(
                    height: MediaQuery.of(context).size.height * 0.1,
                    width: MediaQuery.of(context).size.width * 0.4,
                    title: 'Latitude',
                    value: spotInfo.latitude,
                  ),
                ],
              ),
              SizedBox(height: MediaQuery.of(context).size.height * 0.035),
              DetailsListItem(
                height: MediaQuery.of(context).size.height * 0.1,
                width: double.infinity,
                title: 'Wind Probability',
                value: spotInfo.probability.toString() + ' %',
              ),
              SizedBox(height: MediaQuery.of(context).size.height * 0.035),
              DetailsListItem(
                height: MediaQuery.of(context).size.height * 0.1,
                width: double.infinity,
                title: 'Country',
                value: spotInfo.country,
              ),
              SizedBox(height: MediaQuery.of(context).size.height * 0.035),
              DetailsListItem(
                height: MediaQuery.of(context).size.height * 0.1,
                width: double.infinity,
                title: 'When to Go',
                value: spotInfo.month,
              ),
              SizedBox(height: MediaQuery.of(context).size.height * 0.035),
              CupertinoButton(
                  color: CupertinoColors.extraLightBackgroundGray,
                  child: RichText(
                      text: TextSpan(children: [
                    WidgetSpan(child: Icon(FontAwesomeIcons.share, color: CupertinoColors.systemBlue)),
                    TextSpan(
                        text: '\t\t\tShare this spot',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 16.0,
                            color: CupertinoColors.systemBlue))
                  ])),
                  onPressed: () {
                    _shareSpotUsingText(
                        context,
                        'Hey, you should really consider ${spotInfo.name}' +
                            ' for your next kitesurfing adventure!\nI\'ve found this spot on KiteSurferDemo',
                        '${spotInfo.name}, ${spotInfo.country}');
                  })
            ],
          ),
        ));
  }

  void _shareSpotUsingText(
      BuildContext context, String message, String subject) {
    final RenderBox renderBox = context.findRenderObject();

    Share.share(message,
        subject: subject,
        sharePositionOrigin:
            renderBox.localToGlobal(Offset.zero) & renderBox.size);
  }
}

// template for the list
class DetailsListItem extends StatelessWidget {
  final double width;
  final double height;
  final String title;
  final String value;

  DetailsListItem({Key key, this.height, this.width, this.title, this.value})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        width: width,
        height: height,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              title,
              style: TextStyle(
                  color: CupertinoColors.black,
                  fontSize: 27.0,
                  fontWeight: FontWeight.normal,
                  decoration: TextDecoration.none,
                  fontFamily: 'Georgia'),
            ),
            Text(
              value,
              style: TextStyle(
                  color: CupertinoColors.black,
                  fontSize: 17.0,
                  fontWeight: FontWeight.normal,
                  decoration: TextDecoration.none,
                  fontFamily: 'Georgia'),
              textAlign: TextAlign.left,
            ),
          ],
        ));
  }
}
