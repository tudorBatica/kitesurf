import 'package:flutter/material.dart';
import 'package:ktsurf/models/filters.dart';
import 'package:ktsurf/widgets/gradientroundedbutton.dart';

// this is the filter screen for Android
// when the 'Apply' button is pushed,
// the data (_filters)
// will be send to the list screen

class MaterialFilterScreen extends StatefulWidget {
  
  final List<String> countriesList;
  
  final Filters initialFilters; // this filters that are in place
                                // when this screen launches
                                // we need this if the user decides
                                // to discard the changes (by pushing the back arrow)

  MaterialFilterScreen({Key key, this.countriesList, this.initialFilters}) {
    this.countriesList.sort();
  }

  @override
  _MaterialFilterScreenState createState() => _MaterialFilterScreenState();
}

class _MaterialFilterScreenState extends State<MaterialFilterScreen> {
  
  Filters _filters = Filters();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      // override the back button so that the initial filters
      // are sent back to the list screen
      onWillPop: () async {
        Navigator.pop(context, widget.initialFilters);
        return false;
      },
       child: Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        centerTitle: true,
        title: Text('Filter'),
        // this screen requires that we create the 
        // back button manually, otherwise the filters will not
        // be sent back to the list screen 
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context, widget.initialFilters);
          },
        )
      ),
      body: Column(
        children: <Widget>[
          SizedBox(height: MediaQuery.of(context).size.height * 0.05),
          Padding(
            padding: const EdgeInsets.only(left: 10.0),
            child: Text('Wind Probability', style: TextStyle(fontSize: 18.0)),
          ),
          SizedBox(height: MediaQuery.of(context).size.height * 0.05),
          RangeSlider(
            min: 0.0,
            max: 100.0,
            labels: RangeLabels('${_filters.windRangeValues.start}', '${_filters.windRangeValues.end}'),
            values: _filters.windRangeValues,
            divisions: 100,
            onChanged: (values) {
              setState(() => _filters.windRangeValues = values);
            },
          ),
          SizedBox(height: MediaQuery.of(context).size.height * 0.1),
          Text(
            'Country',
            style: TextStyle(
              fontSize: 18.0,
            ),
          ),
          SizedBox(height: MediaQuery.of(context).size.height * 0.05),
          DropdownButton(
              value: _filters.country,
              items: widget.countriesList.map((String country) {
                return DropdownMenuItem<String>(
                    value: country, child: Text(country));
              }).toList(),
              onChanged: (String newValue) {
                setState(() {
                  _filters.country= newValue;
                });
              }),
          SizedBox(height: MediaQuery.of(context).size.height * 0.1),
          GradientRoundedButton(
            minWidth: MediaQuery.of(context).size.width * 0.4,
            minHeight: MediaQuery.of(context).size.height * 0.065,
            borderRadius: 20.0,
            gradient:
                LinearGradient(colors: [Colors.blueAccent, Colors.blue]),
            text: Text('Apply',
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.white, fontSize: 16)),
            onPressed: () {
              //get the filters from the user,
              // send them to the list screen and pop this screen
              Navigator.pop(context, _filters);
            },
          )
        ],
      )),
    );
  }

}