import 'dart:io';

import 'package:carousel_widget/carousel_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:ktsurf/models/tutorialtip.dart';
import 'package:ktsurf/services/sessioninfo.dart';

// the pop up tutorial that is presented to the user;
// changes the way it looks, according to OS

class NativePopupTutorial extends StatefulWidget {
  final double width;
  final double height;

  // user's name can be provided when calling the popup
  // to give the tutorial a more personalized feel
  final String username;

  final List<TutorialTip> tips;

  NativePopupTutorial({
    Key key,
    this.width,
    this.height,
    this.username = 'Kitesurfer',
    this.tips,
  }) : super(key: key);

  @override
  _NativePopupTutorialState createState() => _NativePopupTutorialState();
}

class _NativePopupTutorialState extends State<NativePopupTutorial> {
  @override
  Widget build(BuildContext context) {
    // android implementation
    if (Platform.isAndroid) {
      return AlertDialog(
        title: _tutorialTitle(),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(20.0))),
        content: _tutorialContent(),
        actions: <Widget>[
          FlatButton(
            child: Text('Close'),
            onPressed: () {
              Navigator.pop(context);
              SessionInfo.stopShowingTutorial();
            }
          )
        ],
      );
    } else {
      // iOS implementation
      return CupertinoAlertDialog(
        title: _tutorialTitle(),
        content: _tutorialContent(),
        actions: <Widget>[
          FlatButton(
            child: Text(
              'Close',
              style: TextStyle(color: CupertinoColors.destructiveRed),
            ),
            onPressed: () {
              Navigator.pop(context);
              SessionInfo.stopShowingTutorial();
            },
          )
        ],
      );
    }
  }

  // creates tutorial's title container
  Container _tutorialTitle() {
    return Container(
      width: widget.width,
      height: widget.height * 0.2,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Text(
            'Hello, ${widget.username}',
            textAlign: TextAlign.center,
          ),
          SizedBox(height: 10.0),
          Text(
            'Here are a couple quick tips to turn you into a pro spot hunter',
            style: TextStyle(color: Colors.grey, fontSize: 15.0),
            textAlign: TextAlign.center,
          )
        ],
      ),
    );
  }

  // creates tutorial's content container
  Container _tutorialContent() {
    return Container(
            width: widget.width,
            height: widget.height * 0.8,
            child: Carousel(
              listViews: _carouselItems(),
            ));
  }

  // converts a list of tutorial tips
  // into a list of fragments that can be used
  // in the carousel widget
  List<Fragment> _carouselItems() {
    var fragments = List<Fragment>();

    for (TutorialTip tip in widget.tips) {
      fragments.add(
        Fragment(
            child: Container(
          child: Column(
            children: <Widget>[
              Text(
                tip.title,
                style: TextStyle(fontWeight: FontWeight.bold),
                textAlign: TextAlign.center,
              ),
              Text(
                tip.tip,
                textAlign: TextAlign.center,
              ),
              SizedBox(height: 20.0),
              Container(
                child: tip.image,
              )
            ],
          ),
        )),
      );
    }

    return fragments;
  }
}
