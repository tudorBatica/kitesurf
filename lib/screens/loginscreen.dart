import 'package:flutter/material.dart';
import 'package:ktsurf/models/user.dart';
import 'package:ktsurf/services/mockapicontroller.dart';
import 'package:ktsurf/services/sessioninfo.dart';
import 'package:ktsurf/widgets/gradientroundedbutton.dart';
import 'package:ktsurf/widgets/waveycontainer.dart';

// app's login screen
class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  //login information provided by user
  String _username;
  String _password;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
            child: Column(
      children: <Widget>[
        Stack(
          children: <Widget>[
            WaveyContainer(
                height: MediaQuery.of(context).size.height * 0.3,
                colors: [
                  Colors.white,
                  Colors.lightBlueAccent,
                  Colors.blue,
                  Colors.blueAccent
                ]),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height * 0.1,
                    right: MediaQuery.of(context).size.width * 0.1,
                  ),
                  child: Text(
                    'the seas\nhave missed you...',
                    textAlign: TextAlign.end,
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 18.0),
                  ),
                )
              ],
            )
          ],
        ),
        SizedBox(height: MediaQuery.of(context).size.height * 0.05),
        Text(
          'Authenticate',
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 25.0,
          ),
        ),
        Container(
          width: MediaQuery.of(context).size.width * 0.8,
          child: Column(
            children: <Widget>[
              SizedBox(height: MediaQuery.of(context).size.height * 0.025),
              //username field
              TextFormField(
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(20.0)),
                  labelText: 'Username',
                ),
                onChanged: (value) {
                  setState(() {
                    _username = value;
                  });
                },
              ),
              SizedBox(height: MediaQuery.of(context).size.height * 0.025),
              //password field
              TextFormField(
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(20.0)),
                  labelText: 'Password',
                ),
                obscureText: true,
                onChanged: (value) {
                  setState(() {
                    _password = value;
                  });
                },
              ),
              SizedBox(height: MediaQuery.of(context).size.height * 0.05),
              GradientRoundedButton(
                minWidth: MediaQuery.of(context).size.width * 0.4,
                minHeight: MediaQuery.of(context).size.height * 0.065,
                gradient:
                    LinearGradient(colors: [Colors.blueAccent, Colors.blue]),
                borderRadius: 20.0,
                text: Text('Sign in',
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.white, fontSize: 16)),
                onPressed: _login,
              )
            ],
          ),
        ),
      ],
    )));
  }

  //login function
  Future<User> _login() async {
    //simulate authentication
    int userId = int.parse(
        await MockAPIController.loginWithUsername(_username, _password));

    //save user data
    User currentUser = await MockAPIController.fetchUserById(userId);
    SessionInfo.userLoggedIn(currentUser);

    Navigator.pop(context);

    return currentUser;
  }
}
