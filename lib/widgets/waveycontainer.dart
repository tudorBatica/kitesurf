 import 'package:flutter/material.dart';

class WaveyContainer extends StatelessWidget {

  final double height;
  final List<Color> colors;

  WaveyContainer({Key key, this.height, this.colors}) : super(key : key);

   @override
   Widget build(BuildContext context) {
     return ClipPath(
       clipper: WaveClipper(),
       child: Container(
         height: height,
         decoration: BoxDecoration(
           gradient: LinearGradient(
             begin: Alignment.bottomCenter,
             end: Alignment.topCenter,
             colors: colors
           )
         ),
       ),
     );
   }
 }
 
// container's clipper (the container
// starts its life as a rectangle and is then clipped
// using the wave clipper)

class WaveClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = new Path();
    path.lineTo(0.0, size.height * 0.7);

    var firstControlPoint = Offset(size.width / 4, size.height);
    var firstEndPoint = Offset(size.width / 2.25, size.height * 0.8);
    path.quadraticBezierTo(firstControlPoint.dx, firstControlPoint.dy, 
                          firstEndPoint.dx, firstEndPoint.dy);
    
    var secondControlPoint = 
                    Offset(size.width - (size.width / 3.6), size.height * 0.5);
    var secondEndPoint = Offset(size.width, size.height - 40);
    path.quadraticBezierTo(secondControlPoint.dx, secondControlPoint.dy, 
                          secondEndPoint.dx, secondEndPoint.dy);
    
    path.lineTo(size.width, size.height - 40);
    path.lineTo(size.width, 0.0);
    path.close();
    
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}