// user model

class User {
  final int userId;

  final String name;
  final String picURL;
  final String description;

  User(
      {this.name,
      this.picURL = 'https://png.pngtree.com/svg/20161212/f93e57629c.png',
      this.description =
          'Hi! I am using KiteSurf Demo to find the best kitesurfing spots in the world!',
      this.userId = 1});

  factory User.fromJson(Map<String, dynamic> json) {
    String name;
    String picURL;
    String description;
    json["name"] != null ? name = json["name"] : name = "Top KiteSurfer";
    json["avatar"] != null
        ? picURL = json["avatar"]
        : picURL = "https://png.pngtree.com/svg/20161212/f93e57629c.png";
    json["description"] != null
        ? description = json["description"]
        : description =
            "Hi! I am using KiteSurf Demo to find the best kitesurfing spots in the world!";

    return User(name: name, picURL: picURL, description: description);
  }
}
