import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ktsurf/models/filters.dart';

class CupertinoFilterScreen extends StatefulWidget {
// this is the filter screen for iOS
// when the 'Save' button is pushed,
// the data (_filters)
// will be send to the list screen

  final List<String> countriesList;

  final Filters initialFilters; // this filters that are in place
  // when this screen launches
  // we need this if the user decides
  // to discard the changes

  CupertinoFilterScreen({Key key, this.countriesList, this.initialFilters}) {
    this.countriesList.sort();
  }

  @override
  _CupertinoFilterScreenState createState() => _CupertinoFilterScreenState();
}

class _CupertinoFilterScreenState extends State<CupertinoFilterScreen> {
  Filters _filters = Filters();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      // override the back slide
      onWillPop: () async => false,
      child: CupertinoPageScaffold(
          navigationBar: CupertinoNavigationBar(
            backgroundColor: CupertinoColors.extraLightBackgroundGray,
            padding: EdgeInsetsDirectional.only(end: 15.0),
            leading: Padding(
              padding: EdgeInsets.only(
                  left: 15.0, top: MediaQuery.of(context).size.height * 0.015),
              child: GestureDetector(
                  child: Text('Discard',
                      style: TextStyle(color: CupertinoColors.activeBlue)),
                  onTap: () {
                    showCupertinoDialog(
                        context: context,
                        builder: (context) {
                          return CupertinoAlertDialog(
                            title: Text('Confirmation'),
                            content: Text('Are you sure you want' +
                                ' to discard the changes made?'),
                            actions: <Widget>[
                              FlatButton(
                                child: Text(
                                  'Cancel',
                                  style: TextStyle(
                                      color: CupertinoColors.activeBlue),
                                ),
                                onPressed: () {
                                  Navigator.of(context).pop(); // close dialog
                                },
                              ),
                              FlatButton(
                                child: Text('Discard',
                                    style: TextStyle(
                                        color: CupertinoColors.destructiveRed)),
                                onPressed: () {
                                  Navigator.of(context).pop(); // close dialog
                                  Navigator.pop(
                                      context,
                                      widget
                                          .initialFilters); // close filters screen
                                },
                              ),
                            ],
                          );
                        });
                  }),
            ),
            middle: Text(
              'Filters',
            ),
            trailing: GestureDetector(
              child: Text('Save',
                  style: TextStyle(color: CupertinoColors.activeBlue)),
              onTap: () {
                Navigator.pop(context, _filters);
              },
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.only(left: 10.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(height: MediaQuery.of(context).size.height * 0.025),
                Text('Wind Probability Minimum',
                    style: TextStyle(
                        fontSize: 25.0,
                        color: CupertinoColors.black,
                        decoration: TextDecoration.none,
                        fontFamily: 'Georgia',
                        fontWeight: FontWeight.normal)),
                SizedBox(height: MediaQuery.of(context).size.height * 0.025),
                Container(
                  width: MediaQuery.of(context).size.width * 0.8,
                  child: CupertinoTextField(
                    clearButtonMode: OverlayVisibilityMode.always,
                    keyboardType: TextInputType.numberWithOptions(),
                    placeholder: 'between 0 and 100',
                    onChanged: (newValue) {
                      if (double.parse(newValue) <= 100.0)
                        _filters.windRangeValues =
                            RangeValues(double.parse(newValue), 100.0);
                      // upper bound is always 100.0 because
                      // on iOS we don't have a range slider for now
                      //TODO implement an iOS range slider
                    },
                  ),
                ),
                SizedBox(height: MediaQuery.of(context).size.height * 0.05),
                Text('Country',
                    style: TextStyle(
                        fontSize: 25.0,
                        color: CupertinoColors.black,
                        decoration: TextDecoration.none,
                        fontFamily: 'Georgia',
                        fontWeight: FontWeight.normal)),
                SizedBox(height: MediaQuery.of(context).size.height * 0.025),
                Center(
                  child: CupertinoButton(
                      child: Text('Select Country'),
                      onPressed: () {
                        showCupertinoModalPopup(
                            context: context,
                            builder: (context) {
                              return Container(
                                height:
                                    MediaQuery.of(context).size.height * 0.5,
                                child: CupertinoPicker.builder(
                                  itemExtent:
                                      MediaQuery.of(context).size.height * 0.1,
                                  onSelectedItemChanged: (value) {
                                    _filters.country =
                                        widget.countriesList[value];
                                    print(_filters.country);
                                  },
                                  childCount: widget.countriesList.length,
                                  itemBuilder: (context, index) {
                                    return Center(
                                      child: Text(
                                        widget.countriesList[index],
                                        style: TextStyle(fontSize: 30.0),
                                        textAlign: TextAlign.center,
                                      ),
                                    );
                                  },
                                ),
                              );
                            });
                      }),
                )
              ],
            ),
          )),
    );
  }
}
