// the screen that displays user's profile
// and a logout button

import 'package:flutter/material.dart';
import 'package:ktsurf/models/user.dart';
import 'package:ktsurf/services/sessioninfo.dart';
import 'package:ktsurf/widgets/gradientroundedbutton.dart';
import 'package:ktsurf/widgets/waveycontainer.dart';

class UserProfileScreen extends StatelessWidget {
  final User user;

  UserProfileScreen({Key key, this.user}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          Stack(
            children: <Widget>[
              WaveyContainer(
                  height: MediaQuery.of(context).size.height * 0.4,
                  colors: [
                    Colors.white,
                    Colors.lightBlueAccent,
                    Colors.blue,
                    Colors.blueAccent
                  ]),
              Padding(
                padding: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height * 0.1),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    CircleAvatar(
                      radius: MediaQuery.of(context).size.width * 0.2,
                      backgroundImage: NetworkImage(user.picURL),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width * 0.5,
                      child: Text(
                        user.name,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 17.0),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(left: 15.0, right: 15.0),
            child: Text('"' + user.description + '"',
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.black, fontSize: 20.0)),
          ),
          SizedBox(height: MediaQuery.of(context).size.height * 0.1),
          Container(
            width: MediaQuery.of(context).size.width * 0.5,
            child: GradientRoundedButton(
              minWidth: MediaQuery.of(context).size.width,
              minHeight: MediaQuery.of(context).size.height * 0.065,
              gradient:
                  LinearGradient(colors: [Colors.blueAccent, Colors.blue]),
              borderRadius: 20.0,
              text: Text('Sign out',
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.white, fontSize: 16)),
              onPressed: () {
                //logout
                SessionInfo.userLoggedOut();
                Navigator.pop(context);
              },
            ),
          )
        ],
      ),
    );
  }
}
