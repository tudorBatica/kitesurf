// a class that holds the values 
// entered by the user in the 
// filter screen

import 'package:flutter/material.dart';

class Filters {

  String country;

  RangeValues windRangeValues;

  Filters({this.country = 'All', this.windRangeValues = const RangeValues(0.0, 100.0)});

}