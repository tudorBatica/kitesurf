// tip model class
// used for the pop up tutorial

import 'package:flutter/widgets.dart';

class TutorialTip {
  final String title;
  final String tip;
  final Image image;

  TutorialTip({this.title, this.tip, this.image});
}
