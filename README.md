# kitesurf

A cross-platform mobile application for kitesurfers.

## Features

The app provides the users with a list of kitesurfing spots.  
Users can:  
- filter the spots by wind probability and country  
- see the spots on Google Maps(only on Android for now)  
- share a spot using Whatsapp, E-mail, Facebook, SMS, pretty much any app that can send text  
- login / logout  

## Project Components

Please note that the data used in the project comes from a Mock API. 

### 1. Screens
All the screens used in this app can be found in thel lib/screens folder.  

#### 1.1. List screen
The first screen is the List Screen that shows the users all the kitesurfing spots.
Each spot can be marked as a favourite spot. Because the API is a mocked API, when 
a spot is added to favourites, it is being added with a randomly generated id, so
don't expect to see the spot marked as favourite when you reload the list screen.  
On this screen's app bar you will find two buttons. If you push the one on the left,
you will be taken to the Filter Screen. The one on the right will take you to 
the Login Screen if no user is logged in, or to the User Profile screen. To see
all the details about a spot, press on it.  

#### 1.2. Details screen
This screen shows all the details about a given spot. If you press the share button
you can share the spot on Whatsapp, Facebook, SMS, etc.

#### 1.3. Filter screen
This screen is used to filter the spots in the List Screen. Spots can be filtered
by wind probability and country. On Android, filtering the wind probability is 
done by providing a range. On iOS it is done by providing a minimum wind probability.
When the apply button is pushed, users will be sent back to the filter screen. The 
filters will be passsed to this screen as well, and the list will be filtered.

#### 1.4. Login screen
Using this screen a user can sign in. After they do that, they will be presented
with a quick tutorial on how to use the app. A user will remain logged in until
they choose to sign out. To accomplish this, information about the current session
are being safely stored using the SessionInfo service class

#### 1.5. User Profile screen
This screen is available once a user logged in. It will display their profile(username,
and description). It also contains a sign out button.

#### 1.6. Map screen
This screen is available only for Android users, for now. To see a spot on the map,
swipe a spot to the right(when in the List Screen). A button will appear, with the
text ('on map'). Press that button and you will be sent to the Map Screen which will
show you the spot on Google Maps.  

### 2. Service classes  
All the service classes used in this app are located in the lib/services folder.  

#### 2.1. MockAPIData  
This service class retrieves data regarding the mocked API(base URL, endpoints) 
from the 'mockapiconfing.json' file.

#### 2.2. MockAPIController
This service class is used to interact with the API(fetches or sends data).

#### 2.3. SessionInfo
This service class is used to safely store data about the current session(
such as user's name, description, image, if the user has seen or not the 
tutorial).  

### 3. Other widgets
Other helpful widgets that I wrote for this app can be found in the lib/widgets folder.  

#### 3.1. SpotsListTile
This is the widget used for displaying the spots in the List Screen.  

#### 3.2. WaveyContainer
A container with one side in shape of a wave.

#### 3.3. NativePopupTutorial
The quick tutorial that is being presented to the user once they've logged in.
The name 'native' suggests that the pop up looks different on iOS and Android, 
trying to conserve the native feel and look.  

#### 3.4. GradientRoundedButton
A button with rounded borders and gradient. 



## Project structure

In the 'lib' folder you will find the following folders:  
    
    - 'screens'  
In this folder you will find all the screens of the app.  
The screens files are named using the following name convention:  
 - the screens whose name begin with 'material' are displayed to Android users  
 - the screens whose name begin with 'cupertino' are displayed to iOS users  
 - the other screens are common screens for both platforms  
 <a/>

    - 'widgets'
This folder contains all the custom made widgets used to create the GUI.  
If a file starts with the name 'native' it means that that widget will have a different look, according to the
operating system of the user.
<a/>


    - 'services'  
This folder contains the dart classes that handle the logic required for the app to interact with the mock API.

<a/>



    - 'assets'
In this folder you will find all the assets that require dart code (custom icons created with FlutterIcon for example)  


