import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class GoogleMapScreen extends StatelessWidget {

  final double longitude;
  final double latitude;

  GoogleMapScreen({Key key, this.longitude, this.latitude}) : super(key : key);

  @override
  Widget build(BuildContext context) {

    Set<Marker> spotMarker = Set<Marker>();
    spotMarker.add(Marker(
      markerId: MarkerId("Spot marker"),
      draggable: false,
      position: LatLng(latitude, longitude),
      onTap: () {
        print('Awesome kite surfing spot');
      }
    ));
    
    return Scaffold(
      body: GoogleMap(
        initialCameraPosition: CameraPosition(
          target: LatLng(latitude, longitude)
        ),
        markers: spotMarker,
      )
    );
  }
}