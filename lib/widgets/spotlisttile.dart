import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:ktsurf/assets/my_flutter_app_icons.dart';
import 'package:ktsurf/models/spot.dart';
import 'package:ktsurf/services/mockapicontroller.dart';

// the tile used for the spots list
// located on the spots screen

class SpotListTile extends StatefulWidget {
  final double width;
  final double height;
  final double borderRadius;
  final double iconSize;
  final Color initialIconColor;

  final Spot spot;

  final Function screenToPush;

  SpotListTile({
    Key key,
    this.width = double.infinity,
    this.height,
    this.spot,
    this.borderRadius = 20.0,
    this.iconSize,
    this.screenToPush,
    this.initialIconColor,
  }) : super(key: key);

  @override
  _SpotListTileState createState() => _SpotListTileState();
}

class _SpotListTileState extends State<SpotListTile> {
  Color _iconColor;

  @override
  void initState() {
    super.initState();

    _iconColor = widget.initialIconColor;
  }

  @override
  Widget build(BuildContext context) {
    // we need an actual value of the width
    // in case the user of this widget did not
    // provide one
    double usableWidthValue;
    if (widget.width == double.infinity)
      usableWidthValue = MediaQuery.of(context).size.width;
    else
      usableWidthValue = widget.width;

    return Container(
        width: widget.width,
        height: widget.height,
        color: Colors.transparent,
        child: Stack(
          children: <Widget>[
            Padding(
                padding: EdgeInsets.only(
                  top: widget.iconSize * 0.5,
                ),
                child: GestureDetector(
                  onTap: widget.screenToPush,
                  child: Container(
                    height: widget.height - widget.iconSize,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          topRight: (Radius.circular(widget.borderRadius)),
                          bottomRight: (Radius.circular(widget.borderRadius))),
                      color: Colors.white,
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey,
                          offset: Offset(-1.0, 1.0),
                          blurRadius: 5.0,
                        ),
                      ],
                    ),
                    child: Row(
                      children: <Widget>[
                        // spot location information section
                        Container(
                          width: usableWidthValue * 0.5,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(widget.borderRadius),
                            topLeft: Radius.circular(widget.borderRadius),
                          )),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Text(
                                widget.spot.name,
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 17.0,
                                ),
                                textAlign: TextAlign.center,
                              ),
                              Text(
                                widget.spot.country,
                                style: TextStyle(
                                    color: Colors.grey, fontSize: 16.0),
                                textAlign: TextAlign.center,
                              )
                            ],
                          ),
                        ),
                        // wind and when to go information section
                        Container(
                            width: usableWidthValue * 0.5,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.only(
                                bottomRight:
                                    Radius.circular(widget.borderRadius),
                                topRight: Radius.circular(widget.borderRadius),
                              ),
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                RichText(
                                  text: TextSpan(children: [
                                    WidgetSpan(
                                        child: Icon(
                                      FontAwesomeIcons.wind,
                                      color: Color.lerp(
                                          Colors.red,
                                          Colors.amber,
                                          widget.spot.probability / 100),
                                    )),
                                    TextSpan(
                                        text: ' ' +
                                            widget.spot.probability.toString() +
                                            ' %',
                                        style: TextStyle(
                                            color: Color.lerp(
                                                Colors.red,
                                                Colors.amber,
                                                widget.spot.probability / 100),
                                            fontSize: 16))
                                  ]),
                                ),
                                SizedBox(height: widget.height * 0.1),
                                RichText(
                                    text: TextSpan(children: [
                                  WidgetSpan(
                                    child: Icon(
                                      FontAwesomeIcons.calendarAlt,
                                      color: Colors.black,
                                    ),
                                  ),
                                  TextSpan(
                                      text: ' ' + widget.spot.month,
                                      style: TextStyle(
                                          color: Colors.black, fontSize: 16))
                                ]))
                              ],
                            ))
                      ],
                    ),
                  ),
                )),
            Row(mainAxisAlignment: MainAxisAlignment.center, children: [
              GestureDetector(
                child: Icon(
                  MyFlutterApp.box,
                  size: widget.iconSize,
                  color: _iconColor,
                ),
                onTap: () {
                  setState(() {
                    if (_iconColor != Colors.amber) {
                      _iconColor = Colors.amber;
                      Fluttertoast.showToast(
                          msg:
                              '${widget.spot.name} added to the treasure chest');
                    } else {
                      _iconColor = Colors.black;
                      Fluttertoast.showToast(
                          msg:
                              '${widget.spot.name} removed from the treasure chest');
                    }
                  });
                  MockAPIController.addFavourite(widget.spot.id);
                },
              )
            ])
          ],
        ));
  }
}
