import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ktsurf/screens/listscreen.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return (Platform.isIOS
        ? CupertinoApp(
            debugShowCheckedModeBanner: false,
            localizationsDelegates: const <LocalizationsDelegate<dynamic>>[
              DefaultCupertinoLocalizations.delegate,
              DefaultMaterialLocalizations.delegate,
              DefaultWidgetsLocalizations.delegate,
            ],
            theme: CupertinoThemeData(
                brightness: Brightness.light,
                primaryColor: CupertinoColors.extraLightBackgroundGray,
                textTheme: CupertinoTextThemeData(
                    textStyle: TextStyle(fontFamily: 'Georgia'))),
            title: 'Kite Surf',
            home: ListScreen())
        : MaterialApp(
            debugShowCheckedModeBanner: false,
            title: 'Kite Surf',
            home: ListScreen(),
            theme: ThemeData(
              primarySwatch: Colors.blue,
              primaryColor: Colors.blue,
              accentColor: Colors.amberAccent,
              fontFamily: 'Georgia',
            ),
        )
    );
  }
}
