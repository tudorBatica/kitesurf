import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:ktsurf/models/spot.dart';
import 'package:share/share.dart';

// this is the details screen that appears
// when users press on a spot on Android

class MaterialDetailsScreen extends StatelessWidget {
  final Spot spotInfo;

  MaterialDetailsScreen({Key key, this.spotInfo}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Theme.of(context).primaryColor,
          centerTitle: true,
          title: Text(
            spotInfo.name,
            style: TextStyle(color: Colors.white, fontSize: 17.0),
          ),
        ),
        body: ListView(
          children: [
            DetailsListItem(
              height: MediaQuery.of(context).size.height * 0.1,
              width: double.infinity,
              title: "Country",
              value: spotInfo.country,
            ),
            DetailsListItem(
                height: MediaQuery.of(context).size.height * 0.1,
                width: double.infinity,
                title: "Latitude",
                value: spotInfo.latitude),
            DetailsListItem(
                height: MediaQuery.of(context).size.height * 0.1,
                width: double.infinity,
                title: "Longitude",
                value: spotInfo.longitude),
            DetailsListItem(
              height: MediaQuery.of(context).size.height * 0.1,
              width: double.infinity,
              title: "Wind Probability",
              value: spotInfo.probability.toString() + '%',
            ),
            DetailsListItem(
              height: MediaQuery.of(context).size.height * 0.1,
              width: double.infinity,
              title: "When to Go",
              value: spotInfo.month,
            ),
            Center(
                child: MaterialButton(
              onPressed: () {
                _shareSpotUsingText(
                    context,
                    'Hey, you should really consider ${spotInfo.name}' +
                        ' for your next kitesurfing adventure!\nI\'ve found this spot on KiteSurferDemo',
                    '${spotInfo.name}, ${spotInfo.country}');
              },
              child: Container(
                height: MediaQuery.of(context).size.height * 0.05,
                width: MediaQuery.of(context).size.width * 0.4,
                decoration: BoxDecoration(
                    color: Theme.of(context).primaryColor,
                    borderRadius: BorderRadius.all(Radius.circular(10.0))),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    RichText(
                        textAlign: TextAlign.center,
                        text: TextSpan(children: [
                          WidgetSpan(
                              child: Icon(
                            FontAwesomeIcons.share,
                            color: Colors.white,
                          )),
                          TextSpan(
                              text: '  Share this spot',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 17.0,
                                  fontWeight: FontWeight.bold))
                        ])),
                  ],
                ),
              ),
            ))
          ],
        ));
  }

  // this method shares a text message (through
  // sms, whatsapp, email ...);
  // in this app it's used to share kitesurfing spots
  void _shareSpotUsingText(
      BuildContext context, String message, String subject) {
    final RenderBox renderBox = context.findRenderObject();

    Share.share(message,
        subject: subject,
        sharePositionOrigin:
            renderBox.localToGlobal(Offset.zero) & renderBox.size);
  }
}

// this class is used to create the list items
// for the details screen

class DetailsListItem extends StatelessWidget {
  final String title;
  final String value;
  final double height;
  final double width;

  DetailsListItem({Key key, this.title, this.value, this.height, this.width})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 8.0),
            child: Text(
              title,
              textAlign: TextAlign.start,
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 17.0),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 8.0),
            child: Text(value,
                textAlign: TextAlign.left,
                style: TextStyle(fontSize: 16, color: Colors.black54)),
          )
        ],
      ),
    );
  }
}
