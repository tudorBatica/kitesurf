import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:ktsurf/models/filters.dart';
import 'package:ktsurf/models/spot.dart';
import 'package:ktsurf/models/tutorialtip.dart';
import 'package:ktsurf/models/user.dart';
import 'package:ktsurf/screens/cupertinodetailsscreen.dart';
import 'package:ktsurf/screens/cupertinofilterscreen.dart';
import 'package:ktsurf/screens/googlemapscreen.dart';
import 'package:ktsurf/screens/materialdetailsscreen.dart';
import 'package:ktsurf/screens/loginscreen.dart';
import 'package:ktsurf/screens/userprofilescreen.dart';
import 'package:ktsurf/services/mockapicontroller.dart';
import 'package:ktsurf/services/sessioninfo.dart';
import 'package:ktsurf/widgets/nativepopuptutorial.dart';
import 'package:ktsurf/widgets/nativesearchbar.dart';
import 'package:ktsurf/widgets/spotlisttile.dart';
import 'package:ktsurf/screens/materialfilterscreen.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:fluttertoast/fluttertoast.dart';

// list screen - provides the user access
// to all kitesurfing spots

class ListScreen extends StatefulWidget {
  @override
  _ListScreenState createState() => _ListScreenState();
}

class _ListScreenState extends State<ListScreen> {
  var _spotsList;
  var _favouriteSpots;

  Filters _filters = Filters(); // retrieved from filter screen

  List<String> _countriesList =
      new List<String>(); // to be passed to the filter screen

  var _appBarBackgroundColor; // depends on the OS

  // this method acts a wrapper: we cannot await the fetchFavourites
  // method in a function that is not async, and we cannot make
  // the initState method async
  Future<void> getFavouriteSpots() async {
    _favouriteSpots = await MockAPIController.fetchFavourites();
  }

  @override
  void initState() {
    super.initState();

    //! please do not move the methods that fetch data from the API
    //! in the build method as they will be called
    //! way too many times

    // retrieve the favourite spots from the database;
    // please note that only the spot ids are retrieved
    getFavouriteSpots();

    // retrieve the spots from the database
    _spotsList = MockAPIController.fetchSpots();

    // after the spots are retrieved,
    // a list of all different countries
    // is created, this information will be passed
    // to the filter screen
    _countriesList.add("All");
    _spotsList.then((spots) {
      for (Spot spot in spots) {
        if (!_countriesList.contains(spot.country.toString()))
          _countriesList.add(spot.country);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    if (Platform.isAndroid)
      _appBarBackgroundColor = Theme.of(context).primaryColor;
    else
      _appBarBackgroundColor = CupertinoColors.extraLightBackgroundGray;

    // push the quick tutorial
    // as of now, the quick tutorial is being shown
    // everytime a new user signs in
    //TODO find a way to show the tutorial only once per installation
    Future.delayed(Duration.zero, () => _showTutorial(context));

    return Scaffold(
        appBar: AppBar(
          backgroundColor: _appBarBackgroundColor,
          elevation: 0.0,
          leading: IconButton(
            onPressed: () => _pushUserScreen(),
            icon: Icon(
              FontAwesomeIcons.user,
              color: Colors.black,
            ),
          ),
          title: NativeSearchBar(
            hintText: 'Type in a spot name',
            iconColor: Colors.grey,
          ),
          actions: [
            IconButton(
              onPressed: () {
                _pushFilterScreen(context);
              },
              icon: Icon(
                Icons.filter_list,
                color: Colors.black,
              ),
            )
          ],
        ),
        body: Column(children: [
          Container(
            height: 20,
          ),
          Expanded(
              child: Container(
                  child: FutureBuilder(
            future: _spotsList,
            builder: (context, snapshot) {
              if (!snapshot.hasData) {
                return Container(
                    child: Center(
                  child: Text('Loading awesome kite surfing spots'),
                ));
              } else {
                return ListView.builder(
                    itemCount: snapshot.data.length,
                    itemBuilder: (context, index) {
                      // when an list tile is created, we must
                      // first check the filters
                      // when a spot is doesn't match an empty
                      // container will be created instead
                      // TODO find a more elegant approach

                      if (_filters.country.toLowerCase() != 'all' &&
                          snapshot.data[index].country.toLowerCase() !=
                              _filters.country.toLowerCase()) {
                        return Container();
                      } else if (_filters.windRangeValues.start >
                              snapshot.data[index].probability ||
                          _filters.windRangeValues.end <
                              snapshot.data[index].probability) {
                        return Container();
                      } else {
                        // if this spot passed the filters,
                        // check if it's a favourite spot
                        Color _treasureChestColor;
                        if (_favouriteSpots.contains(snapshot.data[index].id)) {
                          _treasureChestColor = Colors.amber;
                        } else {
                          _treasureChestColor = Colors.black;
                        }

                        return Slidable(
                            actionPane: SlidableDrawerActionPane(),
                            actionExtentRatio: 0.25,
                            actions: <Widget>[
                              IconSlideAction(
                                  caption: 'On Map',
                                  color: Colors.white,
                                  icon: FontAwesomeIcons.mapMarkedAlt,
                                  onTap: () {
                                    if(Platform.isAndroid) {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                GoogleMapScreen(
                                                  latitude: double.parse(
                                                      snapshot.data[index]
                                                          .latitude),
                                                  longitude: double.parse(
                                                      snapshot.data[index]
                                                          .longitude),
                                                )));
                                    } else {
                                      Fluttertoast.showToast(msg: 'This feature is available only on Android for now');
                                    }
                                  })
                            ],
                            child: SpotListTile(
                              borderRadius: 100.0,
                              height:
                                  MediaQuery.of(context).size.height * 0.175,
                              iconSize:
                                  MediaQuery.of(context).size.height * 0.045,
                              initialIconColor: _treasureChestColor,
                              spot: snapshot.data[index],
                              screenToPush: () {
                                if (Platform.isAndroid) {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              MaterialDetailsScreen(
                                                spotInfo: snapshot.data[index],
                                              )));
                                } else {
                                  Navigator.push(
                                      context,
                                      CupertinoPageRoute(
                                          builder: (context) =>
                                              CupertinoDetailsScreen(
                                                spotInfo: snapshot.data[index],
                                              )));
                                }
                              },
                            ));
                      }
                    });
              }
            },
          )))
        ]));
  }

  // this method pushes the filter screen
  // and awaits the filters set by the user
  void _pushFilterScreen(BuildContext context) async {
    Filters filters = Filters();

    // push the filter screen, according to OS
    if (Platform.isAndroid) {
      filters = await Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => MaterialFilterScreen(
                    countriesList: _countriesList,
                    initialFilters: _filters,
                  )));
    } else {
      filters = await Navigator.push(
          context,
          CupertinoPageRoute(
              builder: (context) => CupertinoFilterScreen(
                    countriesList: _countriesList,
                    initialFilters: _filters,
                  )));
    }

    // retrieve results
    if (filters != _filters) {
      //if changes were made
      setState(() {
        _filters = filters;
      });
    }
  }

  // pushes the login screen if no user is logged in
  // otherwise, pushes the user profile screen
  void _pushUserScreen() async {
    bool isLogged = await SessionInfo.userIsLogged();
    if (isLogged) {
      User user = await SessionInfo.fetchCurrentUser();
      Platform.isAndroid
          ? Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => UserProfileScreen(user: user)))
          : Navigator.push(
              context,
              CupertinoPageRoute(
                  builder: (context) => UserProfileScreen(user: user)));
    } else {
      Platform.isAndroid
          ? Navigator.push(
              context, MaterialPageRoute(builder: (context) => LoginScreen()))
          : Navigator.push(
              context, CupertinoPageRoute(builder: (context) => LoginScreen()));
    }
  }

  // shows the user a quick tutorial
  // after they logged in
  //TODO store data about the user is such way that this tutorial is shown only once for each user

  void _showTutorial(BuildContext context) {
    SessionInfo.fetchCurrentUser().then((currentUser) {
      SessionInfo.userHasSeenTutorial().then((value) {
        if (value) {
          showDialog(
              context: context,
              builder: (context) => NativePopupTutorial(
                    width: MediaQuery.of(context).size.width * 0.8,
                    height: MediaQuery.of(context).size.height * 0.6,
                    username: currentUser.name,
                    tips: [
                      TutorialTip(
                          title: "Treasure chest",
                          tip: "Found a spot you like? Make sure to add it to your treasure chest " +
                              "so you won't lose it. To do this, simply tap the chest icon.",
                          image: Image.asset(
                              'assets/images/favouritetutorial.png')),
                      TutorialTip(
                          title: "Map view",
                          tip: "If you want to see where exactly is a spot, " + 
                              "slide it to the right and tap on 'on map'.",
                          image: Image.asset('assets/images/swipetutorial.png'))
                    ],
                  ));
        }
      });
    });
  }
}
