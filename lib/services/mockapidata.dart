// this class is used to retrieve 
// data about the mock API
// from its config file

import 'dart:convert';
import 'package:flutter/services.dart';

class MockAPIData {
  
  static Future<String> getBaseURL() async {
    final json  = jsonDecode(
      await rootBundle.loadString(
        'assets/config/mockapiconfig.json'
      )
    );

    return json['baseURL'];
  }

  static Future<String> getSpotURL() async {
    final json = jsonDecode(
      await rootBundle.loadString(
        'assets/config/mockapiconfig.json'
      )
    );
    
    return (json['baseURL'] + json['endpoints']['spot']);
  }

  static Future<String> getUserURL() async {
    final json = jsonDecode(
      await rootBundle.loadString(
        'assets/config/mockapiconfig.json'
      )
    );
    
    return (json['baseURL'] + json['endpoints']['user']);
  }

  static Future<String> getFavouritesURL() async {
    final json = jsonDecode(
      await rootBundle.loadString(
        'assets/config/mockapiconfig.json'
      )
    );
    
    return (json['baseURL'] + json['endpoints']['favourites']);
  }

  static Future<String> getLoginURL() async {
    final json = jsonDecode(
      await rootBundle.loadString(
        'assets/config/mockapiconfig.json'
      )
    );
    
    return (json['baseURL'] + json['endpoints']['login']);
  }

}

