// a service that safely stores
// data about the current user

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:ktsurf/models/user.dart';

class SessionInfo {
  static final _secureStorage = FlutterSecureStorage();

  static Future<void> userLoggedIn(User user) async {
    // store data about current user
    await _secureStorage.write(key: 'userLogged', value: 'true');
    await _secureStorage.write(key: 'userName', value: user.name);
    await _secureStorage.write(key: 'userDescription', value: user.description);
    await _secureStorage.write(key: 'userPicURL', value: user.picURL);
    
    // when a user loggs in a tutorial should be shown
    await _secureStorage.write(key: 'showTutorial', value: 'true');
  }

  static void userLoggedOut() async {
    // remember that no user is logged
    _secureStorage.write(key: 'userLogged', value: 'false');
  }

  static Future<bool> userIsLogged() async {
    // check if a user is logged
    String response = await _secureStorage.read(key: 'userLogged');
    bool returnValue = (response == 'true' ? true : false);

    return returnValue;
  }

  static Future<User> fetchCurrentUser() async {
    // retrieve current user information
    User currentUser = User(
        name: await _secureStorage.read(key: 'userName'),
        description: await _secureStorage.read(key: 'userDescription'),
        picURL: await _secureStorage.read(key: 'userPicURL'));

    return currentUser;
  }

  static void stopShowingTutorial() async {
    // user has already seen the tutorial, stop showing it
    await _secureStorage.write(key: 'showTutorial', value: 'false');
  }

  static Future<bool> userHasSeenTutorial() async {
    //check if the user has seen the tutorial
    String response = await _secureStorage.read(key: 'showTutorial');
    bool returnValue = (response == 'true' ? true : false);

    return returnValue;
  }

}
