import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'dart:io';


// a search bar that looks native on 
// android/iOS

class NativeSearchBar extends StatelessWidget {

  final String hintText;
  final Color iconColor;

  NativeSearchBar({Key key, this.hintText, this.iconColor = Colors.grey}): super(key : key);

  @override
  Widget build(BuildContext context) {
    return Platform.isIOS ? CupertinoTextField(
      keyboardType: TextInputType.text,
      placeholder: hintText,
      prefix: Icon(
        Icons.search,
        color: iconColor
      ),
      
    ) : TextField(
      keyboardType: TextInputType.text,
      enableSuggestions: true,
      decoration: InputDecoration(
        hintText: hintText,
        icon: Icon(
          Icons.search,
          color: iconColor,
        )
      ),
    );
  }
}