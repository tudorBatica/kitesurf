// spot model class

class Spot {
  final String month;
  final double probability;
  final String longitude;
  final String latitude;
  final String country;
  final String name;
  final String createdAt;
  final int id;

  Spot(
      {this.month,
      this.country,
      this.createdAt,
      this.latitude,
      this.longitude,
      this.name,
      this.probability,
      this.id});

  factory Spot.fromJson(Map<String, dynamic> json) {
    return Spot(
        month: json['month'],
        probability: json['probability'].toDouble(),
        longitude: json['long'],
        latitude: json['lat'],
        country: json['country'],
        name: json['name'],
        createdAt: json['createdAt'],
        id: int.parse(json['id']));
  }
}
