import 'package:flutter/material.dart';

// a rounded button that also provides 
// a background gradient
// I recommend using this button throughout 
// the application

class GradientRoundedButton extends StatelessWidget {

  final double minWidth;
  final double minHeight;
  
  final Gradient gradient;
  final double borderRadius;

  final Text text;

  final Function onPressed;
  

  GradientRoundedButton(
    {Key key, this.gradient, this.borderRadius, this.minHeight,
    this.minWidth, this.text, this.onPressed}
  ) : super(key : key);

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
        minWidth: minWidth,
        color: Colors.transparent,
        elevation: 0.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(borderRadius)
        ),
        child: Ink(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(borderRadius),
                gradient: gradient),
            child: Container(
              constraints: BoxConstraints(
                minWidth: minWidth,
                minHeight: minHeight,
              ),
              alignment: Alignment.center,
              child: text
            )),
        onPressed: onPressed);
  }
}
