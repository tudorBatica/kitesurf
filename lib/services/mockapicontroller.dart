import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:ktsurf/models/spot.dart';
import 'package:ktsurf/models/user.dart';
import 'package:ktsurf/services/mockapidata.dart';

// this class handles the interaction
// with the mock API

class MockAPIController {
  // fetch all the spots
  static Future<List<Spot>> fetchSpots() async {
    final spotsResponse = await http.get(await MockAPIData.getSpotURL());

    if (spotsResponse.statusCode != 200) {
      throw new Exception("Error while fetchin spots");
    }
    // a 200 response code means all went well
    var spotsList = jsonDecode(spotsResponse.body) as List;
    List<Spot> spotObjects = spotsList
        .map((spotJsonObject) => Spot.fromJson(spotJsonObject))
        .toList();

    return spotObjects;
  }

  // simulate login with username and password
  // using a http post request
  // returns the user id
  static Future<String> loginWithUsername(
      String username, String password) async {
    // map login info
    var loginInfo = Map<String, String>();
    loginInfo['username'] = username;
    loginInfo['password'] = password;

    // make post request
    return http
        .post(await MockAPIData.getLoginURL(), body: loginInfo)
        .then((http.Response response) {
      if (response.statusCode < 200 ||
          response.statusCode > 400 ||
          json == null) {
        throw new Exception("Error while fetching logindata");
      }

      return json.decode(response.body)["userId"].toString();
    });
  }

  // return user info for the given user id
  static Future<User> fetchUserById(int userId) async {
    final String userUrl =
        await MockAPIData.getUserURL() + '/' + userId.toString();
    final userResponse = await http.get(userUrl);

    if (userResponse.statusCode != 200) {
      throw new Exception("Error while fetching user");
    }

    return User.fromJson(json.decode(userResponse.body));
  }

  // fetch favourite spots
  // this method return only the ids of those spots
  static Future<List<int>> fetchFavourites() async {
    final favResponse = await http.get(await MockAPIData.getFavouritesURL());

    if (favResponse.statusCode != 200) {
      throw new Exception("Error while fetching favourites");
    }

    var favList =
        jsonDecode(favResponse.body) as List; //the list of all fav spots

    // from fav list, get all the spot ids and return that list
    List<int> favIdsList = List<int>();

    for (var favSpot in favList) {
      if (!favIdsList.contains(favSpot["spot"])) // data may contain duplicates
        favIdsList.add(favSpot["spot"]);
    }

    return favIdsList;
  }

  // add a spot to favourite spots
  // return true if successful
  static Future<bool> addFavourite(int spotIndex) async {
    var spotToAdd = Map<String, int>();
    spotToAdd['spot'] = spotIndex;
    String encoded = json.encode(spotToAdd); // can't post a Map<String, int>
    // so we need to encode it

    return http
        .post(await MockAPIData.getFavouritesURL(), body: encoded)
        .then((http.Response response) {
      if (response.statusCode < 200 ||
          response.statusCode > 400 ||
          json == null) {
        return false;
      } else {
        return true;
      }
    });
  }

  // removes a spot from favourites, 
  // returns true if successful
  static Future<bool> removeFromFavourites(int index) async {
    //! this method is for demo purposes only, it is not
    //! used anywhere in the app, because everytime a
    //! spot is added to the favorites that spot's id
    //! is not stored, instead the mock API generates
    //! a random spot id for that entry
    //! therefore, it would be redundant to actually use
    //! this method in the application
    
    //! however, this method functions properly and will
    //! delete the favourite spot entry with the id 'index'
    
    String targetURL = await MockAPIData.getFavouritesURL();
    targetURL += '/' + index.toString();

    final deleteResponse = await http.delete(targetURL);
    if(deleteResponse.statusCode == 200) {
      return true;
    } else {
      return false;
    }

  }

}
